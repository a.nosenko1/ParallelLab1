#include <iostream>
#include <cmath>
#include <malloc.h>
#include <time.h>
#include <stdlib.h>
#include <openmpi/mpi.h>
int ProcNum, ProcRank;
int numRowsAtThisProc;  // кол-во строк матрицы в этом процессе
const int N = 130;
const double
        Pi = 3.14159265358979311600e+00,
        epsilon = 1e-5,
        little_epsilon = 1e-10;

void matrixVectorMulParallel(const int matrixSize, const double *matrix, const double *vector,
                             double *procResult, int startPosition, int stopPosition){
    for (int i = startPosition; i <= stopPosition; ++i) { // 0 1 2 3 4
        procResult[i] = 0;
        for (int j = 0; j < matrixSize; ++j) {
            procResult[i] += *(matrix+matrixSize*i+j-startPosition*N) * vector[j];
        }
    }
}
void matrixVectorMul(const int matrixSize, const double *matrix, const double *vector, double *result){
    for (int i = 0; i < matrixSize; ++i) {
        result[i] = 0;
        for (int j = 0; j < matrixSize; ++j) {
            result[i] += *(matrix+matrixSize*i+j) * vector[j];
        }
    }
}
void scalarVectorMul(const int vectorSize, const double scalar, const double *vector, double *result){
    for (int i = 0; i < vectorSize; ++i) {
        result[i] = vector[i] * scalar;
    }
}
void vectorVectorSubParallel(const int vectorSize, const double *vector1, const double *vector2, double *result){
    for (int i = 0; i < vectorSize; ++i) {
        result[i] = vector1[i] - vector2[i];
    }
}
void vectorVectorSub(const int vectorSize, const double *vector1, const double *vector2, double *result){
    for (int i = 0; i < vectorSize; ++i) {
        result[i] = vector1[i] - vector2[i];
    }
}
double vectorVectorScalarMul(const int vectorSize, const double *vector1, const double *vector2){
    double result = 0.0; // 0_0
    for (int i = 0; i < vectorSize; ++i) {
        result += vector1[i] * vector2[i];
    }
    return result;
}
double vectorLength(const int vectorSize, const double *vector){
    double result = 0.0;
    for (int i = 0; i < vectorSize; ++i) {
        result += vector[i]*vector[i];
    }
    return sqrt(result);
}
void copyFirstVectorToSecond(const int vectorSize, const double *vector1, double *vector2){
    for (int i = 0; i < vectorSize; ++i) {
        vector2[i] = vector1[i];
    }
}
void matrixPrint(const int matrixSize, const double *matrix){
    for (int i = 0; i < matrixSize; ++i) {
        printf("    ( ");
        for (int j = 0; j < matrixSize; ++j) {
            printf("%3.2f ", *(matrix+matrixSize*i+j));
        }
        printf(")\n");
    }
}
void vectorPrint(const int vectorSize, const double *vector){
    printf("( ");
    for (int i = 0; i < vectorSize; ++i) {
        printf("%3.2f ", *(vector+i));
    }
    printf(")\n");
}
void initFixedDecision(const int matrixSize, double *matrix, double *vectorB, double *vectorX){
    for (int i = 0; i < matrixSize; ++i) {
        *(vectorB+i) = matrixSize + 1;
        *(vectorX+i) = 0;
        for (int j = 0; j < matrixSize; ++j) {
            if (i == j){
                *(matrix+i*matrixSize+j) = 2.0;
            } else{
                *(matrix+i*matrixSize+j) = 1.0;
            }
        }
    }
}
void randomSimMatrix(const int matrixSize, double *matrix);
void initProisvolDecision(const int matrixSize, double *matrixA, double *vectorB, double *vectorX){
    double* u = (double*)calloc(matrixSize, sizeof(double));
    randomSimMatrix(N, matrixA);
    for (int i = 0; i < matrixSize; ++i) {
        u[i] = sin(2*Pi/matrixSize);
    }
    matrixVectorMul(matrixSize, matrixA, u, vectorB);
  //  vectorPrint(matrixSize, u);
    free(u);
}
void initManual(const int matrixSize, double *matrixA, double *vectorB, double *vectorX){
    double* u = (double*)calloc(matrixSize, sizeof(double));
    printf("%s", "print matrix:\n");
    for (int i = 0; i < matrixSize; ++i) {
        u[i] = sin(2*Pi/matrixSize);
        *(vectorX+i) = 0;
        for (int j = 0; j < matrixSize; ++j) {
            scanf("%lf", (matrixA+i*matrixSize+j));
        }
    }
    printf("%s", "print vector b:\n");
    for (int i = 0; i < matrixSize; ++i) {
        scanf("%lf", (vectorB+i));
    }
    free(u);
}
void printResult(const int size, const int count, const double *matrixA, const double *vectorB, const double *vectorX){
    printf("%s\n", "--------RESULT--------");
    printf("%s %d\n", "iterations count =", count);
    printf("%s", "x = ");
    vectorPrint(size, vectorX); // result
    printf("%s", "b = ");
    vectorPrint(size, vectorB);
    printf("%s\n", "A = ");
    matrixPrint(size, matrixA);
    printf("%s\n", "------RESULT END------");
}
void randomSimMatrix(const int matrixSize, double *matrix){
    for (int i = 0; i < N; i++) {
        matrix[i * N + i] = (double)(rand() % 18 - 9); //диагональ
    }
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < i; j++)
        {
            matrix[i * N + j] = (double)(random() % 18 - 9);
            matrix[j * N + i] = matrix[i * N + j];
        }
    }
}
int main(int argc,char *argv[]) {
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);

    double* A;
    double* b = (double*)calloc(N, sizeof(double));
    double* x = (double*)calloc(N, sizeof(double));
    if (ProcRank == 0){
        A = (double*)calloc(N*N, sizeof(double));  // матрица только в нулевом
        //initFixedDecision(N, A, b, x);
        initProisvolDecision(N, A, b, x);
        //initManual(N, A, b, x, result);
    }
    //отправим всем вектора b и х

    MPI_Bcast(b, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(x, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);


    double* Ax = (double*)calloc(N, sizeof(double));
    double* y = (double*)calloc(N, sizeof(double));
    double* Ay = (double*)calloc(N, sizeof(double));
    double* tau_y = (double*)calloc(N, sizeof(double));
    double* x_n_plus_one = (double*)calloc(N, sizeof(double));
    double tempValueForCompare = 1;
    double tau;
    int count = 0;

    /* сколько строк дать каждому, максимально поровну */
    int restRows = N;
    for (int i = 0; i < ProcRank; ++i) {
        restRows = restRows - restRows/(ProcNum - i);
    }
    numRowsAtThisProc = restRows / (ProcNum - ProcRank);

    /* массив для содержания кол-ва строк матрицы для каждого процесса */
    /* для i-го процесса [i] == кол-ву строк в нем */
    int *countRowsAtProc = new int [ProcNum]; // кол-во строк в процессе
    int *countSymAtProc = new int [ProcNum]; // кол-во символов в процессе
    MPI_Status s;
    for (int i = 0; i < ProcNum; ++i) {
        MPI_Sendrecv(&numRowsAtThisProc, 1, MPI_INTEGER, i, 0,
                     (countRowsAtProc+i), 1, MPI_INTEGER, i, 0,
                     MPI_COMM_WORLD, &s);
    }
    /* теперь есть информация для разделения строк по процессам */
    /* вычисляем начальный и конечный номер строки  */
    int startRowPosition = 0;
    int stopRowPosition;
    for (int i = 0; i < ProcRank; ++i) {
        startRowPosition += countRowsAtProc[i];
    }
    stopRowPosition = startRowPosition + numRowsAtThisProc - 1;
    int *shift = new int [ProcNum]; // сдвиг относительно начала == начальная позиция
    int *shiftMat = new int [ProcNum]; // сдвиг относительно начала в матрице
    for (int i = 0; i < ProcNum; ++i) {
        MPI_Sendrecv(&startRowPosition, 1, MPI_INTEGER, i, 0,
                     (shift+i), 1, MPI_INTEGER, i, 0,
                     MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
    }
    /* Отправим процессам их строки из 0-го процесса */

    for (int i = 0; i < ProcNum; ++i) {
        countSymAtProc[i] = countRowsAtProc[i]*N;
        shiftMat[i] = shift[i]*N;
    }
    double *part_A = new double [countRowsAtProc[ProcRank]*N];
    MPI_Scatterv(A, countSymAtProc, shiftMat, MPI_DOUBLE,
                 part_A, countSymAtProc[ProcRank], MPI_DOUBLE, 0, MPI_COMM_WORLD);

    delete [] shiftMat;
    delete [] countSymAtProc;

    double *buffer = new double [N];
    struct timespec start, stop;
    if (ProcRank == 0) clock_gettime(CLOCK_REALTIME, &start);
    while ((tempValueForCompare - epsilon) > 0){
        count++;

        matrixVectorMulParallel(N, part_A, x, Ax, startRowPosition, stopRowPosition);  // Ax = A * x;
        MPI_Allgatherv((Ax+shift[ProcRank]), countRowsAtProc[ProcRank], MPI_DOUBLE, Ax, countRowsAtProc, shift, MPI_DOUBLE, MPI_COMM_WORLD);
      //  copyFirstVectorToSecond(N, buffer, Ax);

        vectorVectorSub(N, Ax, b, y);  // y = Ax - b;

        matrixVectorMulParallel(N, part_A, y, Ay, startRowPosition, stopRowPosition); // Ay = A * y;
        MPI_Allgatherv((Ay+shift[ProcRank]), countRowsAtProc[ProcRank], MPI_DOUBLE, Ay, countRowsAtProc, shift, MPI_DOUBLE, MPI_COMM_WORLD);
      //  copyFirstVectorToSecond(N, buffer, Ay);

            if (((vectorVectorScalarMul(N, Ay, Ay) - little_epsilon) < 0))
                tau = 0.0;
            else
                tau = vectorVectorScalarMul(N, y, Ay) / vectorVectorScalarMul(N, Ay, Ay);

            scalarVectorMul(N, tau, y, tau_y);           // tau_y = tau * y;
            vectorVectorSub(N, x, tau_y, x_n_plus_one);  // x_n_plus_one = x - tau*y

            if (((vectorLength(N, b) - little_epsilon) < 0))    // ||Ax - b||/||b|| < E
                tempValueForCompare = 0.0;
            else tempValueForCompare = vectorLength(N, y)/vectorLength(N, b);

        MPI_Barrier(MPI_COMM_WORLD);
        copyFirstVectorToSecond(N, x_n_plus_one, x);
    }
    if (ProcRank == 0) {
        clock_gettime(CLOCK_REALTIME, &stop);
        std::cout << stop.tv_sec - start.tv_sec << std::endl;
        std::cout << count << std::endl;
    }

//    if (ProcRank == 0)
//        printResult(N, count, A, b, x);  // printing result

    free(x);
    if (ProcRank == 0){
        free(A);
    }
    free(b);
    free(Ax);
    free(y);
    free(Ay);
    free(tau_y);
    free(x_n_plus_one);
    MPI_Finalize();
    return 0;
}
